<?php return array(
    'root' => array(
        'name' => 'wpsyntex/polylang-pro',
        'pretty_version' => '3.6.x-dev',
        'version' => '3.6.9999999.9999999-dev',
        'reference' => '12dc783720ba8cadc49c7146db46a12ef4517855',
        'type' => 'wordpress-plugin',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => false,
    ),
    'versions' => array(
        'wpsyntex/polylang' => array(
            'pretty_version' => '3.6.x-dev',
            'version' => '3.6.9999999.9999999-dev',
            'reference' => '803654e02a6f2e018f832efd4ef660b433d6249a',
            'type' => 'wordpress-plugin',
            'install_path' => __DIR__ . '/../wpsyntex/polylang',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'wpsyntex/polylang-pro' => array(
            'pretty_version' => '3.6.x-dev',
            'version' => '3.6.9999999.9999999-dev',
            'reference' => '12dc783720ba8cadc49c7146db46a12ef4517855',
            'type' => 'wordpress-plugin',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
